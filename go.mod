module gitlab.com/brandocorp/pingpong

go 1.15

require (
	contrib.go.opencensus.io/exporter/stackdriver v0.13.4
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	go.opencensus.io v0.22.5
)

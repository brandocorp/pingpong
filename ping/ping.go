package ping

import (
	"log"
	"net/http"

	"contrib.go.opencensus.io/exporter/stackdriver/propagation"
	"go.opencensus.io/plugin/ochttp"
)

// Server is a ping server.
type Server struct {
	pongHost string
}

// NewServer returns a new ping server.
func NewServer(pongHost string) Server {
	return Server{
		pongHost: pongHost,
	}
}

// ServeHTTP serves the HTTPs.
func (s Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		s.Handler(w, r)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}

// Handler handles ping requests.
func (s Server) Handler(w http.ResponseWriter, r *http.Request) {
	var count string

	query := r.URL.Query()
	if len(query["count"]) == 1 {
		count = query["count"][0]
	} else {
		count = ""
	}

	switch count {
	case "":
		log.Println("Count is empty. Defaulting to count of one.")
		count = "1"
	case "0":
		log.Println("Count is zero. End of ping-pong.")
		w.Write([]byte(`{"ping":true}`))
		return
	}

	log.Println("Count: " + count)

	client := &http.Client{
		Transport: &ochttp.Transport{
			// Use Google Cloud propagation format.
			Propagation: &propagation.HTTPFormat{},
		},
	}

	pongURL := s.pongHost + "/pong?count=" + count
	req, _ := http.NewRequest("GET", pongURL, nil)
	req = req.WithContext(r.Context())

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		w.Write([]byte(`{"ping":false}`))
		return
	}

	resp.Body.Close()

	w.Write([]byte(`{"ping":true}`))
}

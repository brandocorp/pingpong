package cmd

import (
	"log"
	"net/http"
	"os"

	"contrib.go.opencensus.io/exporter/stackdriver"
	"contrib.go.opencensus.io/exporter/stackdriver/propagation"
	"github.com/spf13/cobra"
	"gitlab.com/brandocorp/pingpong/ping"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/trace"
)

var pingPort string
var pongHost string

// pingCmd represents the ping command
var pingCmd = &cobra.Command{
	Use:   "ping",
	Short: "Run the ping server.",
	Long:  `Run the ping server.`,
	Run:   pingServer,
}

func init() {
	rootCmd.AddCommand(pingCmd)

	pingCmd.Flags().StringVar(&pingPort, "port", "8080", "The port to use for the ping server.")
	pingCmd.Flags().StringVar(&pongHost, "pong", "http://localhost:8080", "The pong server.")
}

func pingServer(cmd *cobra.Command, args []string) {
	log.Println("Starting ping server...")
	log.Println("Pong Server: " + pongHost)
	exporter, err := stackdriver.NewExporter(stackdriver.Options{
		ProjectID: os.Getenv("GOOGLE_CLOUD_PROJECT"),
	})
	if err != nil {
		log.Fatal(err)
	}

	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	srv := ping.NewServer(pongHost)

	http.Handle("/ping", srv)

	httpHandler := &ochttp.Handler{
		Propagation: &propagation.HTTPFormat{},
	}

	if err := http.ListenAndServe(":"+pingPort, httpHandler); err != nil {
		log.Fatal(err)
	}
}

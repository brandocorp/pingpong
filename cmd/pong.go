package cmd

import (
	"log"
	"net/http"
	"os"

	"contrib.go.opencensus.io/exporter/stackdriver"
	"contrib.go.opencensus.io/exporter/stackdriver/propagation"
	"github.com/spf13/cobra"
	"gitlab.com/brandocorp/pingpong/pong"
	"go.opencensus.io/plugin/ochttp"
	"go.opencensus.io/trace"
)

var pongPort string
var pingHost string

// pongCmd represents the pong command
var pongCmd = &cobra.Command{
	Use:   "pong",
	Short: "Run the pong server.",
	Long:  `Run the pong server.`,
	Run:   pongServer,
}

func init() {
	rootCmd.AddCommand(pongCmd)

	pongCmd.Flags().StringVar(&pongPort, "port", "8080", "The port to use for the pong server.")
	pongCmd.Flags().StringVar(&pingHost, "ping", "http://localhost:8080", "The ping server.")
}

func pongServer(cmd *cobra.Command, args []string) {
	log.Println("Starting pong server...")
	log.Println("Ping Server: " + pingHost)

	exporter, err := stackdriver.NewExporter(stackdriver.Options{
		ProjectID: os.Getenv("GOOGLE_CLOUD_PROJECT"),
	})
	if err != nil {
		log.Fatal(err)
	}

	trace.RegisterExporter(exporter)
	trace.ApplyConfig(trace.Config{DefaultSampler: trace.AlwaysSample()})

	srv := pong.NewServer(pingHost)

	http.HandleFunc("/pong", srv.Handler)

	httpHandler := &ochttp.Handler{
		Propagation: &propagation.HTTPFormat{},
	}

	if err := http.ListenAndServe(":"+pongPort, httpHandler); err != nil {
		log.Fatal(err)
	}
}

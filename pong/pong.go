package pong

import (
	"log"
	"net/http"
	"strconv"

	"contrib.go.opencensus.io/exporter/stackdriver/propagation"
	"go.opencensus.io/plugin/ochttp"
)

// Server represents a pong server.
type Server struct {
	pingHost string
}

// NewServer returns a new pong server.
func NewServer(pingHost string) Server {
	return Server{
		pingHost: pingHost,
	}
}

// ServeHTTP serves the HTTPs.
func (s Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Handler(w, r)
}

// Handler handles pong requests.
func (s Server) Handler(w http.ResponseWriter, r *http.Request) {
	var param string
	// Get the count
	query := r.URL.Query()

	if len(query["count"]) == 1 {
		param = query["count"][0]
	} else {
		param = ""
	}

	log.Println("Starting Count: " + param)

	count, err := parseAndDecrementCount(param)
	if err != nil {
		log.Fatal(err)
		return
	}

	client := &http.Client{
		Transport: &ochttp.Transport{
			// Use Google Cloud propagation format.
			Propagation: &propagation.HTTPFormat{},
		},
	}

	// Resend the request with the new count
	pingURL := s.pingHost + "/ping?count=" + count
	req, _ := http.NewRequest("GET", pingURL, nil)
	req = req.WithContext(r.Context())

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		w.Write([]byte(`{"pong":false}`))
		return
	}

	resp.Body.Close()

	w.Write([]byte(`{"pong":true}`))
}

func parseAndDecrementCount(c string) (string, error) {
	var count int
	var err error

	count, err = strconv.Atoi(c)
	if err != nil {
		return "", err
	}

	count = count - 1

	result := strconv.Itoa(count)
	log.Println("Resulting Count: " + result)

	return result, nil
}
